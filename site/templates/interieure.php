<?php namespace ProcessWire;

include("./elements/fonctions.inc");
include("./elements/entete.inc");
?>
<div id="contenu">
  <?php /** @var \ProcessWire\ProcessWire $page */
  $options = array(
    'upscaling' => TRUE,
    'cropping' => TRUE
  );
  if ($page->images):
    $ph = $page->images->first();
    if ($ph->height < 380) {
      $marge = intval((380 - intval($ph->height)) / 2);
      $style = ' style="padding-top:' . $marge . 'px;padding-bottom:' . $marge . 'px"';
    }
    else {
      $style = '';
    }
    $m = $ph;

    if ($ph->width() > 560) {
      $m = $ph->width(560,$options);
    }
    if ($m->height() > 560) {
      $m = $m->height(560,$options);
    }

    ?>
      <div id="photo"><img <?php echo $style; ?> src="<?php echo $m->url; ?>"
                                                 width="<?php echo $m->width ?>"
                                                 height="<?php echo $m->height ?>"
                                                 alt=""/></div>
  <?php endif; ?>
  <?php if ($page->body || $ph->description): ?>
      <div id="texte">
        <?php if (!$page->body) {
          echo $ph->description;
        }
        else {
          echo $page->body;
        } ?>
      </div>
    <?php
  endif;
  // Mots clés
  $lesTags = array();
  $images = $page->images->find("tags!=''");
  if (count($images)) {
    echo '<div id="motsCles"><h2>Mots clés</h2><div id="bteCles">';
    foreach ($images as $im) {
      $tags = $im->tags;
      $tags = explode(' ', $tags);
      foreach ($tags as $tag) {
        $lesTags[$sanitizer->text($tag)][] = $im->url;
      }
    }
    ksort($lesTags);
    while (current($lesTags)) {
      $unTag = normalizeChars(strtolower($sanitizer->text(key($lesTags))));
      echo '<div class="tag" id="' . $unTag . '">' . key($lesTags) . '</div>';
      next($lesTags);
    }
    echo '<div class="tout" id="tous">Tout</div>';
    echo '<div class="legende">Cliquez sur les mots-clés pour filtrer les photos.</div>';
    echo '</div></div>';
  } ?>


    <div id="theme">
      <?php echo $page->theme; ?>
    </div>
    <div id="navTags">
      <?php $homepage = $page->parent;
      $children = $homepage->children;
      foreach ($children as $child) {
        $iden = str_replace('/', '', $child->url);
        $iden = str_replace('-', '', $iden);
        if ($child == $page) {
          echo "<div id='{$iden}' class='choisi'>{$child->title}</div>";
        }
        else {
          echo "<div id='{$iden}' class='normal'><a href='{$child->url}'>{$child->title}</a></div>";
        }
      } ?>
    </div>

</div>
<div id="boiteVerte">
  <?php if ($page->images): ?>
  <?php $n = count($page->images) * 105;
  if ($n > 945) {
    echo '<div id="navG">&lt;</div>';
    echo '<div id="navD">&gt;</div>';
    $style = "";
  }
  else {
    $style = ' style="margin-left:10px;margin-right:5px;width:945px"';
  } ?>
    <div id="conteneurPhotos"<?php echo $style; ?>>
        <div id="ruban" style="width:<?php echo $n; ?>px">
          <?php foreach ($page->images as $ph) {
            $l = $ph->width(100);
            $m = $ph;

            if ($ph->width() > 560) {
              $m = $ph->width(560,$options);
            }
            if ($m->height() > 560) {
              $m = $m->height(560,$options);
            }
            ?>
              <div class="miniature"><img
                          class="<?php echo normalizeChars(strtolower($sanitizer->text($ph->tags))) ?>"
                          src="<?php echo $l->url ?>"
                          title="<?php echo $ph->url ?>" width="100"
                          height="<?php echo $l->height ?>"
                          alt="<?php echo $ph->description ?>"
                          l="<?php echo $m->width(); ?>"
                          h="<?php echo $m->height(); ?>"/></div>
          <?php }
          endif; ?>
        </div>
    </div>
    <div id="pagination"></div>
    <div id="debug"></div>
</div>
<?php include("./elements/pied.inc"); ?>
