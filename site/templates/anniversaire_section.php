<?php namespace ProcessWire;

include("./elements/fonctions.inc");
include("./elements/entete.inc");
?>
<div id="contenu">
	<div id="navTags">
		<?php 
		$homepage = $page->parent; 
		$enfants = $homepage->children;
		foreach($enfants as $enfant) {
			$lapage = $enfant->url;
			$iden = str_replace('/','', $lapage);
			$iden = str_replace('-','', $iden);
			if ($page->parent == $enfant || $page == $enfant){
				$cl= "choisi";
				}else{
				$cl= "normal";
				}
			echo "<div id='{$iden}' class='{$cl}'><a href='{$lapage}'>{$enfant->title}</a></div>";
		}
		?>
	</div>
<div id="navGauche">
<?php 
		$enfants = $page->children;
		foreach($enfants as $enfant) {
			$iden = str_replace('/','', $lapage);
			$iden = str_replace('-','', $iden);
			echo "<div id='{$iden}'><a href='{$enfant->url}'>{$enfant->title}</a></div>";
		}
 ?>
</div>
<div id="temoignage">	
<?php echo $page->body; ?>	
<?php if($page->name=='50e-anniversaire'): ?>
    <video width="700"  controls poster="http://familleverville.org/videos/message.jpg">
        <source src="http://familleverville.org/videos/messageenfants.mp4"  type="video/mp4">
        <source src="http://familleverville.org/videos/messageenfants.webm" type="video/webm"/>
        <source src="http://familleverville.org/videos/messageenfants.ogg" type="video/ogg"/>
        Your browser does not support the video tag.
    </video>

<?php endif; ?>

    <?php if($page->name=='60e-anniversaire'): ?>
        <video width="700"  controls poster="http://familleverville.org/videos/famille2017video.jpg">
            <source src="http://familleverville.org/videos/famille2017video.mp4"  type="video/mp4">
            Your browser does not support the video tag.
        </video>

	<?php endif; ?>
</div>
	<?php
	if(count($page->images) > 0){
	echo '<div id="auteur">'.$page->images->first()->url.'</div>';
	}
	?>

	<div id="theme">
		<?php echo $page->theme; ?>
	</div>
</div>
<div id="boiteVerte">&nbsp;</div>
</div>
	<?php include("./elements/pied.inc");  ?>
