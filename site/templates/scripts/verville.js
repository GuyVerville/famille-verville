$(document).ready(function() {
	// nothing here yet
	var nbreSeq;
	var seq = 0;
	var nbrePhotos;

	function calculerPhotos() {
		if (nbrePhotos > 9) {
			if (nbrePhotos % 8 === 0) {
				nbreSeq = nbrePhotos / 8;
			} else {
				nbreSeq = +parseInt(nbrePhotos / 8, 10) + 1;
			}
		} else {
			nbreSeq = 1;
		}
		$('#navG').css('display', 'none');
		$('#navD').css('display', 'none');
		if (nbrePhotos > 8) {
			$('#navD').css('display', 'block');
		}
		$('#pagination').text('Page 1 de ' + nbreSeq);
		$('#debug').text(nbrePhotos + ' photos');
	}

	function faireDiapo(photos) {
		var lesPhotos = photos.find('img');
		nbrePhotos = lesPhotos.length;
		calculerPhotos();
		lesPhotos.each(function(inc) {
			$(this).click(function() {
				var lien = $(this).attr('title');
				var larg = parseInt($(this).attr('l'), 10);
				var haut = parseInt($(this).attr('h'), 10);
				var marge;
				if (haut < 380) {
					marge = parseInt((380 - haut) / 2, 10) + 'px';
				} else {
					marge = '5px';
				}
				$('#photo').html('<img />');
				$('#photo img').attr({
					'width': larg,
					'height': haut,
					'style': 'padding-top:' + marge + ';padding-bottom:' + marge,
					'src': lien
				});
				if (larg > 460) {
					$('#photo').width(larg);
					$('#texte').width(890 - larg);
					$('#motsCles').width(890 - larg);
				} else {
					$('#photo').width(460);
					$('#texte').width(340);
					$('#motsCles').width(400);
				}
				if (inc > 0) {
					$('#texte').text($(this).attr('alt'));
				}
				//var position = $('#texte').position();
			//	$('#motsCles').css('left', larg+700+'px');
			});
			lesPhotos.eq(0).click();
		});
		//======================
		//DIAPORAMA
		var ruban = $('#ruban');
		var decal = 0;
		var posi = 0;
		$('#navD').click(function() {
			if (seq === 0) {
				$('#navG').show();
			}
			seq++;
			if (seq == nbreSeq - 1) {
				$('#navD').hide();
			}
			posi -= 840;
			decal = posi + 'px';
			ruban.animate({
				left: decal
			}, 'slow');
			$('#pagination').text('Page ' + (seq + 1) + ' de ' + nbreSeq);
		});
		$('#navG').click(function() {
			if (seq > 0) {
				$('#navD').show();
			}
			seq--;
			if (seq < 1) {
				$('#navG').hide();
			}
			posi += 840;
			decal = posi + 'px';
			ruban.animate({
				left: decal
			}, 'slow');
			$('#pagination').text('Page ' + (seq + 1) + ' de ' + nbreSeq);
		});
	}
	if ($('conteneurPhotos').html() !== null) {
		var enPhotos = $('#conteneurPhotos .miniature');
		var rubanHTML = $('#ruban').html();
		faireDiapo(enPhotos);
		$('#tous').click(function() {
			enPhotos.css('display', 'block');
			nbrePhotos = enPhotos.length;
			calculerPhotos();
		});
		$('.tag').each(function() {
			$(this).click(function() {
				$('#ruban').css('left', 0);
				enPhotos.css('display', 'block');
				var cl = '.' + $(this).attr('id');
				var nonPhotos = enPhotos.find('img').not(cl);
				var tagPhotos = enPhotos.find('img' + cl);
				tagPhotos.eq(0).click();
				nonPhotos.parent().css('display', 'none');
				nbrePhotos = tagPhotos.length;
				calculerPhotos();
			});
		});
	}
	
});