$(document).ready(function() {
	// nothing here yet
		var options = [];
		function faireMosaique(laSource){
		$('#diaporama').html('.');
		var srcjSON = $(laSource).find('photo');
		var uneGallerie = [];
		srcjSON.each(function(inc) {
			uneGallerie.push({
				src: $(this).find('src').text(),
				alt: $(this).find('title').text(),
				width: $(this).find('width').text(),
				height: $(this).find('height').text(),
				caption: $.trim($(this).find('tag').text())
			})
		})
		options = {
			input: 'json',
			gallery: uneGallerie,
			external_links: false,
			columns: 5,
			padding: 1,
			// lightbox settings
			modal_name: 'prettyPhoto',
			modal_ready_callback: function($pm) {
				$('a[rel^="prettyPhoto"]', $pm).prettyPhoto({
					overlay_gallery: false,
					slideshow: 5000,
					theme: "pp_default",
					deeplinking: false,
					social_tools: "",
					show_title: true,
					default_width: 800,
					default_height: 800,
					opacity: 0.8
				});
			}
		}
		$('#diaporama').removeData('photoMosaic').empty().photoMosaic(options);
		}
		 /* fin photomosaic */
		 	if($('#motsClesIndex').html() !== null){
	
		faireMosaique('#source famille')
		$('#famille').addClass('choisi');
		$('.tag').each(function() {
			$(this).click(function() {
				$('.tag').removeClass('choisi');
				$(this).addClass('choisi');
				faireMosaique('#source '+$(this).attr('id'));
			})
		})
	}

});