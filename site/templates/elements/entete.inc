<!DOCTYPE html>
<?php 
define('CHEIMG', $config->urls->templates.'styles/images/');
define('CHETMPCSS', $config->urls->templates.'styles/') ; 
define('CHETMPJS', $config->urls->templates.'scripts/') ; 
$homepage = $pages->get("/"); 
?>
<html lang="fr">
<head>
<meta charset="utf-8">
	<title><?php echo $page->get("headline|title"); ?></title>
	<meta name="description" content="<?php echo $page->summary; ?>" />
	<meta name="generator" content="ProcessWire <?php echo $config->version; ?>" />
<?php 	if($page->template=="indextags"): ?>	
		<link rel="stylesheet" href=<?php echo CHETMPJS.'photomosaic/includes/prettyPhoto/prettyPhoto.css' ?> title="" type="text/css" media="screen" charset="utf-8">
		<link rel="stylesheet" href=<?php echo CHETMPJS.'photomosaic/css/photoMosaic.css' ?> title="" type="text/css" media="screen" charset="utf-8">
	<?php endif; ?>
	<link rel="stylesheet" type="text/css" href="<?php echo CHETMPCSS?>verville.css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/ie.css" /><![endif]-->	
	<script type="text/javascript">
  (function() {
    var config = {
      kitId: 'igp1ohz',
      scriptTimeout: 3000
    };
    var h=document.getElementsByTagName("html")[0];h.className+=" wf-loading";var t=setTimeout(function(){h.className=h.className.replace(/(\s|^)wf-loading(\s|$)/g," ");h.className+=" wf-inactive"},config.scriptTimeout);var tk=document.createElement("script"),d=false;tk.src='//use.typekit.net/'+config.kitId+'.js';tk.type="text/javascript";tk.async="true";tk.onload=tk.onreadystatechange=function(){var a=this.readyState;if(d||a&&a!="complete"&&a!="loaded")return;d=true;clearTimeout(t);try{Typekit.load(config)}catch(b){}};var s=document.getElementsByTagName("script")[0];s.parentNode.insertBefore(tk,s)
  })();
</script>
	<script type="text/javascript" src="<?php echo CHETMPJS?>jquery-3.2.1.min.js"></script>
	<?php if($page->template=="indextags"): ?>	
		<script type="text/javascript" src=<?php echo CHETMPJS.'photomosaic/includes/prettyPhoto/jquery.prettyPhoto.js' ?>></script>
		<script type="text/javascript" src=<?php echo CHETMPJS.'photomosaic/js/jquery.photoMosaic.js' ?>></script>
		<script type="text/javascript" src=<?php echo CHETMPJS.'mosaique.js' ?>></script>
	<?php endif; ?>
	<?php if($page->name=='50e-anniversaire'): ?>
	<script type="text/javascript" src="//cdn.sublimevideo.net/js/jnz8q5fl.js"></script>
	<?php endif; ?>	
	<script type="text/javascript" src="<?php echo CHETMPJS?>verville.js"></script>
</head>
<body class="<?php if ($page->name == "home"){
		echo 'frontale';
	}elseif ($page->template == "anniversaire"){
		echo 'anniversaire';
	}elseif ($page->template == "anniversaire_section"){
		echo 'anniversairesection';
	}else{
		echo 'interieure';}?>">
<div class="enveloppe">
<div class="suzy">
	<div id="bandeauSup">
		<div class="chapeau"><a href="<?php echo $config->urls->admin; ?>"><img src="<?php echo CHEIMG; ?>chapeau.png" alt="" width="300" height="16" /></a></div>
		<div class="nom"><a href="<?php echo $homepage->url; ?>"><img id="verville" src="<?php echo CHEIMG; ?>nomlogo.png" alt="" width="597" height="31" /></a>
		</div>
		<?php if($page->name != "home"): ?>	
			<div id="navigation">
				<ul>
					<?php 				
					$enfants = $homepage->children;
					foreach($enfants as $enfant) {
						if($enfant->numChildren){
							$lapage = $enfant->children->first()->url;
						}
						else{
							$lapage = $enfant->url;
						}
						$iden = str_replace('/','', $lapage);
						$iden = str_replace('-','', $iden);
						if ($page->parent->parent == $enfant || $page->parent == $enfant || $page == $enfant){
							$cl= "choisi";
							}else{
							$cl= "normal";
							}
						echo "<li id='{$iden}' class='{$cl}'><a href='{$lapage}'>{$enfant->title}</a></li>";
					}
					echo '<li><a href="'.$homepage->url.'">Accueil</a></li>';
					 ?>
				</ul>
			</div>
			<?php endif; ?>




			
