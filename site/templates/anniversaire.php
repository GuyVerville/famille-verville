<?php namespace ProcessWire;

include("./elements/fonctions.inc");
include("./elements/entete.inc");
?>
<div id="contenu">
	<div id="navTags">
		<?php 
		$homepage = $page->parent->parent; 
		$enfants = $homepage->children;
		foreach($enfants as $enfant) {
			$lapage = $enfant->url;
			$iden = str_replace('/','', $lapage);
			$iden = str_replace('-','', $iden);
			if ($page->parent == $enfant || $page == $enfant){
				$cl= "choisi";
				}else{
				$cl= "normal";
				}
			echo "<div id='{$iden}' class='{$cl}'><a href='{$lapage}'>{$enfant->title}</a></div>";
		}
		?>
	</div>
<div id="navGauche">
<?php 
		$parent = $page->parent;
		$enfants = $parent->children;
		foreach($enfants as $enfant) {
			$iden = str_replace('/','', $lapage);
			$iden = str_replace('-','', $iden);
			if ($page->parent == $enfant || $page == $enfant){
				$cl= "choisi";
				}else{
				$cl= "normal";
				}
			echo "<div id='{$iden}' class='{$cl}'><a href='{$enfant->url}'>{$enfant->title}</a></div>";
		}
 ?>
</div><div id="temoignage">	
<h1><?php echo $page->sujet ?></h1>
<?php echo $page->body; ?>	
</div>
	<?php
	if($page->images){
	echo '<div id="auteur"><img src="'.$page->images->first()->url.'" /></div>';
	}
	?>

	<div id="theme">
		TÉMOIGNAGES
	</div>
</div>
<div id="boiteVerte">&nbsp;</div>
</div>
	<?php include("./elements/pied.inc");  ?>
