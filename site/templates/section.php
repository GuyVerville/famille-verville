<?php namespace ProcessWire;

/**
 * Home template
 *
 */

include("./elements/entete.inc"); 
?>
<div id="contenu">
	<div id="intro"><?php echo $page->body; ?></div>
	<div id="theme">
		<?php echo $page->theme; ?>
	</div>
</div>
<div id="boiteVerte">&nbsp;</div>
<?php include("./elements/pied.inc");  ?>

