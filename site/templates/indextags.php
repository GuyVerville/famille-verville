<?php namespace ProcessWire;

/**
 * Home template
 *
 */
include("./elements/fonctions.inc");
include("./elements/entete.inc");
?>
    <div id="contenu">
		<h1>&nbsp;</h1>
        <div id="motsClesIndex">
        <?php echo $page->body; ?>
            <h2>Mots clés</h2>
            <div id="bteCles">
<?php                
                // Mots clés

                $lesTags = array();
                $accueil = $pages->get('/');
                $section = $accueil->children;
				foreach ($section as $lp){                
	                $parray = $lp->find("sort=sort,images.tags!=''"); 
	                foreach($parray as $p) { 
	                    $images = $p->images->find("tags!=''");
	                    if(count($images)){
	                        foreach($images as $im) {
	                            $tags = $im->tags;
	                            $tags = explode(' ', $tags);
	                            foreach($tags as $tag){
	                            	if($tag == '50e'){$tag = 'z50e';}
	                                $data = array(
	                                    "n" => $tag,
	                                    "d" => $im->description,
	                                    "u" => $im->url,
	                                    "l" => $im->width(),
	                                    "h" => $im->height()
	                                );
	                                $lesTags[$sanitizer->text($tag)][] = $data;
	                            }
	                        }
							} 
	                    }
	                }
                       ksort($lesTags);
                       while (current($lesTags)) {
                           $unTag = normalizeChars(strtolower($sanitizer->text(key($lesTags))));
                           if(key($lesTags)=='z50e'){$t = '50e';}else{$t=key($lesTags);}
                           echo '<div class="tag" id="'.$unTag.'">'.$t.'</div>' ;
                           next($lesTags);
                       } 
?>
                <!-- <div class="tag tout" id="tous">Tout</div> -->
            </div>
        </div>
        <div id="theme">
            <?php echo $page->theme; ?>
        </div>
    </div>

    <div id="diaporama"></div><?php 
        $arbre = '<div id="source"><?xml version="1.0" encoding="ISO-8859-1"?><photos>'; 
        foreach($lesTags as $leTag) {
        	$nom = normalizeChars(strtolower($sanitizer->text($leTag[0]['n']))); 
        	$arbre .= '<'.$nom.'>'; 
        	foreach ($leTag as $t){ 
        		$arbre .= '<photo><title>'.$t['d'].'</title>';
        		$arbre .= '<src>'.$t['u'].'</src>';
        		$arbre .= '<width>'.$t['l'].'</width>';
        		$arbre .= '<height>'.$t['h'].'</height></photo>'; 
				} 
        	$arbre .= '</'.$nom.'>';
        	}
			$arbre .= '</photos></div>';
        	echo $arbre ;
?>

    <div id="boiteVerte"></div><?php include("./elements/pied.inc");  ?>
</body>
</html>
