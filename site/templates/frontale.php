<?php namespace ProcessWire;

include("./elements/entete.inc"); 
?>
<div id="contenu">
	<div id="intro"><?php echo $page->body; ?></div>
	<div id="theme">
		<?php echo $page->theme; ?>
	</div>
	<div id="sectionFrontale">
	<?php $enfants = $page->children;
	$i=0;
		foreach($enfants as $enfant){
		$i++;
				if($enfant->numChildren){
					$lapage = $enfant->children->first()->url;
				}
				else{
					$lapage = $enfant->url;
				}
			echo '<div class="bulle"><div class="titre">';
			$n = $enfant->title;
			if($i==1 || $i==3){
				$n = str_replace(' ','<br/>', $n);}
			echo '<a href="'.$lapage.'">'.$n.'</div>';
			echo '<div class="photo"><img src="'.$enfant->images->first()->url.'" /></a></div></div>';
		}
		
	 ?>
	</div>
</div>
<div id="boiteVerte">&nbsp;</div>
<?php include("./elements/pied.inc");  ?>

